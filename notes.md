2.1 Вам потрібно додати нове домашнє завдання в новий репозиторій на Gitlab.

1. На сайті Gitlab.com виконати Create new../New project/Create blank project
2. Заповнюю Project name та встановлюю конфігурації Visibility level = public. Тисну Create project.
3. Копіюю URL на репозиторій за шляхом Сlone/Clone with HTTPS
4. За допомогою Visual Studio Code відкриваю папку з домашнім завданням на компьютері за шляхом File/Open folder..
5. Відкриваю термінал за посиланням Terminal/New terminal
6. У терміналі вводжу команду git clone і вставляю скопійоване посилання. Enter
7. В папці з домашнім завданням створюється папка з назвою створенного в Gitlab репозиторію. Відкриваю цю папку за допомогою File/Open folder..
8. Виконую домашнє завдання в цій папці, або за допомогою провідника на компьютері переміщую до цієї папки готове ДЗ
9. По завершенню в терміналі вводжу команди:
    git add .
    git commit -m "New HW"
    git push
10. Оновлюю сторінку репозиторія в Gitlab, чим перевіряю виконання завдання

2.2 Вам потрібно додати існуюче домашнє завдання в новий репозиторій на Gitlab.

1. На сайті Gitlab.com виконати Create new../New project/Create blank project
2. Заповнюю Project name та встановлюю конфігурації Visibility level = public. Тисну Create project.
3. Копіюю URL на репозиторій за шляхом Сlone/Clone with HTTPS
4. За допомогою Visual Studio Code відкриваю папку з виконаним домашнім завданням на компьютері за шляхом File/Open folder..
5. Відкриваю термінал за посиланням Terminal/New terminal
6. У терміналі вводжу команди:
    git init
    git add .
    git commit -m "New HW2"
7. У терміналі вводжу команду git remote add origin і вставляю скопійоване посилання. Enter
8. Створилась адреса з репозиторієм з назвою origin. Відправляю данні на цей репозиторій за допомогою команди git push origin main. Де main це назва гілки.
9. Оновлюю сторінку репозиторія в Gitlab, чим перевіряю виконання завдання
    